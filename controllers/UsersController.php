<?php

/**
 * Users Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

use App;
use Exception;
use Illuminate\Http\Request;

class UsersController extends BaseController {

    /**
     * Gets list of all the users
     * @param Illuminate\Http\Request $request
     * @return string
     */
    public function getList(Request $request) {

        $data = [];

        try {

            $users = App::make('User')->orderBy('username')->get();

            if (!empty($value = $request->input('keyword'))) {

                $users = App::make('User')->where('firstname', 'like', '%' . $value . '%')
                    ->orWhere('lastname', 'like', '%' . $value . '%')
                    ->orWhere('username', 'like', '%' . $value . '%')
                    ->orWhere('email', 'like', '%' . $value . '%')->orderBy('username')->get();
            }

            if ($users) {

                foreach ($users as $user) {

                    $data[] = [
                        'id'        =>  $user->getId(),
                        'fullname'  =>  $user->getFullname(),
                        'username'  =>  $user->getUsername(),
                        'email'     =>  $user->getEmail()
                    ];
                }
            }
        }
        catch (Exception $e) {

            $data['error'] = $e->getMessage();
        }

        return response()->json($data);
    }

    /**
     * Displays user's view page
     * @param int $id
     * @return Illuminate\View\View
     */
    public function viewUser($id) {

        $user = App::make('User')->where('id', (int)$id)->first();

        return view('scrum::pages.users.user')->with(compact('user'));
    }
}
