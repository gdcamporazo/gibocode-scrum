<?php

/**
 * Error Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

class ErrorController extends \App\Http\Controllers\Controller {

    /**
     * Shows the error page
     * @return Illuminate\Http\Response
     */
    public function view() {

        $data = [
            'title'   => 'Page Not Found',
            'code'    => 404,
            'message' => 'Page Not Found'
        ];

        return response()->view('scrum::pages.error', compact('data'), $data['code']);
    }
}
