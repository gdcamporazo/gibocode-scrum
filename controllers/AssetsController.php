<?php

/**
 * Assets Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

class AssetsController extends \App\Http\Controllers\Controller {

    /**
     * Gets the response based on the path and file format of the asset requested
     * @param string $path
     * @param string $file
     * @return Illuminate\Http\Response
     */
    public function get($path, $file) {

        $filepath = dirname(__DIR__) . '/public/' . $path . '/' . $file;
        $response = '';

        if (file_exists($filepath)) {

            $response = response(file_get_contents($filepath))->header('Content-Type', content_type($path, $file));
        }

        return $response;
    }
}
