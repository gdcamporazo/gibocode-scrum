<?php

/**
 * Issues Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

use App;
use Exception;
use Illuminate\Http\Request;

class IssuesController extends BaseController {

    /**
     * Gets all boards with board ID and name
     * @return string
     */
    public function getBoards() {

        try {

            $boards = App::make('Board')->select('board_id', 'name')->orderBy('name', 'asc')->get();

            $result = $boards->toJson();
        }
        catch (Exception $e) {

            $result = response()->json(['error' => $e->getMessage()]);
        }

        return $result;
    }

    /**
     * Gets all assignees with assignee id and username
     * @return string
     */
    public function getAssignee() {

        try {

            $assignee = App::make('User')->select('id', 'username')->orderBy('username', 'asc')->get();

            $result = $assignee->toJson();
        }
        catch (Exception $e) {

            $result = response()->json(['error' => $e->getMessage()]);
        }

        return $result;
    }

    /**
     * Gets the list of issues
     * @param Illuminate\Http\Request $request
     * @return string
     */
    public function getList(Request $request) {

        $data = [];

        try {

            $issues = App::make('Issue');

            $show = !empty($show = $request->input('show')) ? (int)$show : 50;
            $page = !empty($page = $request->input('page')) ? (int)$page : 1;

            $fields = ['keyword', 'board', 'title', 'priority', 'type', 'status', 'assignee']; // Allowed fields to include in the query

            foreach ($request->all() as $field => $value) {

                if (in_array($field, $fields)) {

                    if ($field == 'keyword') {

                        $issues = $issues->where('title', 'like', '%' . $value . '%');
                        continue;
                    }

                    if ($field == 'assignee') {

                        if (empty($value)) {

                            $issues = $issues->where('assignee', 'size', 0);
                            continue;
                        }

                        $value = (int)$value;
                    }

                    $issues = $issues->where($field, $value);
                }
            }

            $total_issues = $issues->count();
            $issues = $issues->skip($show * ($page - 1))->take($show)->get();

            if ($issues) {

                foreach ($issues as $issue) {

                    $data[] = [
                        'issue_id'          =>  $issue->getIssueId(),
                        'board'             =>  $issue->getBoard(),
                        'title'             =>  $issue->getTitle(),
                        'priority'          =>  $issue->getPriority(),
                        'type'              =>  $issue->getType(),
                        'status'            =>  $issue->getStatus(),
                        'assignee'          =>  $issue->getAssigneeByUsername(),
                    ];
                }

                $data['page'] = $page;
                $data['total_pages'] = floor($total_issues / $show) + ((($total_issues % $show) > 0) ? 1 : 0);
                $data['total_issues'] = $total_issues;
            }
        }
        catch (Exception $e) {

            $data['error'] = $e->getMessage();
        }

        return response()->json($data);
    }

    /**
     * Gets issue object with comments object
     * @param Illuminate\Http\Request $request
     * @return string
     */
    public function getIssue(Request $request) {

        try {

            $issue_id = $request->input('issue_id');

            $issue = App::make('Issue')->where('issue_id', $issue_id)->first();
            $comments = App::make('Comment')->where('issue_id', $issue_id)->orderBy('created_at', 'desc')->get();

            $data = [
                'issue'     =>  $issue,
                'comments'  =>  $comments
            ];
        }
        catch (Exception $e) {

            $data['error'] = $e->getMessage();
        }

        return response()->json($data);
    }

    /**
     * Saves issue
     * @param Illuminate\Http\Request $request
     * @return string
     */
    public function saveIssue(Request $request) {

        try {

            $data = $request->input('data');

            $issue = App::make('Issue')->where('issue_id', $data['issue_id'])->first();

            if ($issue) {

                foreach ($data as $field => $value) {

                    $issue->{$field} = $value;
                }

                $issue->save();
            }
        }
        catch (Exception $e) {

            $data['error'] = $e->getMessage();
        }

        return response()->json($data);
    }

    /**
     * Saves a comment
     * @param Illuminate\Http\Request $request
     * @return string
     */
    public function submitComment(Request $request) {

        try {

            $issue_id = $request->input('issue_id');
            $content = $request->input('content');

            $data = [
                'issue_id'  =>  $issue_id,
                'author'    =>  auth()->user()->username,
                'content'   =>  $content
            ];

            App::make('Comment')->create($data);
        }
        catch (Exception $e) {

            $data['error'] = $e->getMessage();
        }

        return response()->json($data);
    }

    /**
     * Deletes comment
     * @param Illuminate\Http\Request $request
     * @return string
     */
    public function deleteComment(Request $request) {

        $data = [];

        try {

            $comment_id = $request->input('comment_id');

            App::make('Comment')->find($comment_id)->delete();
        }
        catch (Exception $e) {

            $data['error'] = $e->getMessage();
        }

        return response()->json($data);
    }
}
