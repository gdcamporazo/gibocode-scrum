<?php

/**
 * Register Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Gibocode\Scrum\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;

class RegisterController extends BaseController {

    /**
     * Register Controller Constructor
     * @return void
     */
    public function __construct() {

        $this->middleware(\Gibocode\Scrum\Middleware\AdminRedirect::class);
    }

    /**
     * Registers the new user
     * @param Illumintae\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function register(Request $request) {

        $validationRules = [
            'email'             =>  'bail|required|email|unique:users',
            'username'          =>  'bail|required|unique:users|min:3|max:25',
            'password'          =>  'bail|required|max:25|confirmed',
            'registration-code' =>  'bail|required|in:' . registration_code()
        ];

        $validationMessage = ['registration-code.in' => 'Invalid registration code.'];

        if ($this->validate($request, $validationRules, $validationMessage)) {

            try {

                $data = $request->all();
                $data['password'] = Hash::make(trim($data['password']));
                User::create($data);

                return redirect()->route('scrum-login')->with('success', 'Registration completed!');
            }
            catch (QueryException $e) {

                $message = 'Database error.';
            }

            throw ValidationException::withMessages(['message' => (isset($message) ? $message : 'Registration failed.')]);
        }

        return redirect()->back()->withInput();
    }
}
