<?php

/**
 * Base Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

use App;
use Illuminate\Http\Request;

class BaseController extends \App\Http\Controllers\Controller {

    /**
     * Base Controller Constructor
     * @return void
     */
    public function __construct() {

        config(['auth.defaults.guard' => 'scrum_users']);   // Overrides the default user authentication

        $this->middleware('scrum.auth');
    }

    /**
     * Shows a page dynamically
     * @return Illuminate\View\View
     */
    public function index(Request $request) {

        $route = $request->route()->getName();
        $route = str_replace('scrum-', '', $route);

        return view("scrum::pages.{$route}");
    }
}
