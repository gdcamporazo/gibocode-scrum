<?php

/**
 * Login Controller
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;

class LoginController extends BaseController {

    /**
     * Login Controller Constructor
     * @return void
     */
    public function __construct() {

        $this->middleware('scrum.admin')->except('logout');
    }

    /**
     * Logins user
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function login(Request $request) {

        $validationRules = [
            'username'  =>  'required',
            'password'  =>  'required'
        ];

        if ($this->validate($request, $validationRules)) {

            try {

                if (Auth::guard('scrum_users')->attempt($request->only('username', 'password')) ||
                    Auth::guard('scrum_users')->attempt(['email' => $request->username, 'password' => $request->password])) {

                    return redirect()->route('scrum-dashboard');
                }
            }
            catch (QueryException $e) {

                $message = 'Database error.';
            }

            throw ValidationException::withMessages(['message' => (isset($message) ? $message : 'Invalid username or password.')]);
        }

        return redirect()->back()->withInput();
    }

    /**
     * Logouts user
     * @return Illuminate\Http\RedirectResponse
     */
    public function logout() {

        Auth::guard('scrum_users')->logout();

        return redirect()->route('scrum-login');
    }
}
