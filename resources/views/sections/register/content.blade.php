<form method="POST" action="{{ route('scrum-register') }}">
    @csrf
    <div class="register-container">
        <h3>Register</h3>

        @if ($errors->any())
            <div class="alert alert-danger errors">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @break
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="firstname-field-container">
            <label for="firstname">First Name</label>
            <input type="text" class="form-control" name="firstname" autofocus value="{{ old('firstname') }}"/>
        </div>
        <div class="lastname-field-container">
            <label for="lastname">Last Name</label>
            <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}"/>
        </div>
        <div class="email-field-container">
            <label for="email"><span>*</span> Email</label>
            <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" required value="{{ old('email') }}"/>
        </div>
        <div class="username-field-container">
            <label for="username"><span>*</span> Username</label>
            <input type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" required value="{{ old('username') }}"/>
        </div>
        <div class="password-field-container">
            <label for="password"><span>*</span> Password</label>
            <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" required value="{{ old('password') }}"/>
        </div>
        <div class="confirm-password-field-container">
            <label for="password_confirmation"><span>*</span> Confirm Password</label>
            <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password_confirmation" required/>
        </div>
        <div class="registration-code-field-container">
            <label for="registration-code"><span>*</span> Registration Code</label>
            <input type="text" class="form-control {{ $errors->has('registration-code') ? 'is-invalid' : '' }}" name="registration-code" required/>
        </div>
        <div class="action-field-container">
            <input type="submit" class="btn btn-success" value="Register"/>
            <a href="{{ route('scrum-login') }}">Back to Login</a>
        </div>
    </div>
</form>
