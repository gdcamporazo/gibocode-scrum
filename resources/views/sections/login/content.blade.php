<form method="POST" action="{{ route('scrum-login') }}">
    @csrf
    <div class="login-container">
        <div class="banner-container">
            <img src="{{ asset_path('banner.jpg', 'images') }}"/>
        </div>
        <h3>Login</h3>

        @if ($errors->any())
            <div class="alert alert-danger errors">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @break
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif

        <div class="username-field-container">
            <label for="username"><span>*</span> Username or Email</label>
            <input type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" required autofocus value="{{ old('username') }}"/>
        </div>
        <div class="password-field-container">
            <label for="password"><span>*</span> Password</label>
            <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" required/>
        </div>
        <div class="action-field-container">
            <input type="submit" class="btn btn-primary" value="Login"/>
            <a href="{{ route('scrum-register') }}">Register</a>
        </div>
    </div>
</form>
