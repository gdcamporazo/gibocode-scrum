<div class="error-container">
    <div>Error {{ $data['code'] }}</div>
    <div>{{ $data['message'] }}</div>
    <div>
        <a href="{{ route('scrum-dashboard') }}">Go back to home</a>
    </div>
</div>
