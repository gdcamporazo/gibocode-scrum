<div class="issue-container">
    <div class="issue-details">
        <div class="issue-id-container">
            <div class="issue-id"></div>
        </div>
        <hr>
        <div class="issue-title-container">
            <label for="issue-title"><span>*</span> Title</label>
            <span class="issue-title-counter">0/50</span>
            <input type="text" class="form-control issue-title" maxlength="50"/>
        </div>
        <div class="issue-description-container">
            <label for="issue-description">Description</label>
            <textarea class="form-control issue-description"></textarea>
        </div>
        <div class="issue-type-container">
            <label for="issue-type"><span>*</span> Type</label>
            <input type="button" class="form-control issue-type-box-container"/>
            <input type="hidden" class="issue-type" value=""/>
            <div class="issue-type-selected-item"></div>
            <div class="issue-type-list">
                <div class="issue-type-item" data-issue-type="bug">
                    <i class="issue-type-icon fas fa-egg type-bug"></i>Bug
                </div>
                <div class="issue-type-item" data-issue-type="task">
                    <i class="issue-type-icon fas fas fa-flag type-task"></i>Task
                </div>
                <div class="issue-type-item" data-issue-type="story">
                    <i class="issue-type-icon fas fa-bookmark type-story"></i>Story
                </div>
                <div class="issue-type-item" data-issue-type="epic">
                    <i class="issue-type-icon fas fa-star type-epic"></i>Epic
                </div>
                <div class="issue-type-item" data-issue-type="theme">
                    <i class="issue-type-icon fas fa-heart type-theme"></i>Theme
                </div>
            </div>
        </div>
        <div class="issue-priority-container">
            <label for="issue-priority"><span>*</span> Priority</label>
            <input type="button" class="form-control issue-priority-box-container"/>
            <input type="hidden" class="issue-priority" value=""/>
            <div class="issue-priority-selected-item"></div>
            <div class="issue-priority-list">
                <div class="issue-priority-item" data-issue-priority="highest">
                    <i class="priority-icon fas fa-arrow-up priority-highest"></i>Highest
                </div>
                <div class="issue-priority-item" data-issue-priority="high">
                    <i class="priority-icon fas fa-arrow-up priority-high"></i>High
                </div>
                <div class="issue-priority-item" data-issue-priority="medium">
                    <i class="priority-icon fas fa-minus priority-medium"></i>Medium
                </div>
                <div class="issue-priority-item" data-issue-priority="low">
                    <i class="priority-icon fas fa-arrow-down priority-low"></i>Low
                </div>
                <div class="issue-priority-item" data-issue-priority="lowest">
                    <i class="priority-icon fas fa-arrow-down priority-lowest"></i>Lowest
                </div>
            </div>
        </div>
        <div style="width:100%;text-align:right;margin-top:20px;">
            <button type="button" class="btn btn-primary issue-save-button">Save Changes</button>
            <button type="button" class="btn btn-secondary issue-cancel-button">Cancel</button>
        </div>
        <div class="submit-comment-container">
            <label for="issue-comments">Write a comment</label>
            <textarea class="form-control new-comment" style="height:70px;"></textarea>
            <div style="width:100%;text-align:right;">
                <button type="button" class="btn btn-success submit-comment disabled">Submit Comment</button>
            </div>
        </div>
    </div>
    <div class="issue-comments-section">
        <div class="comments-counter"></div>
        <hr>
        <div class="comments-list"></div>
    </div>
</div>
