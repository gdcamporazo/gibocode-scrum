<div class="issues-content">
    <h4>All Issues</h4>
    <div class="content-top-section">
        <div class="filters">
            <span>Filters: </span>
            <a href="#" class="filter-item" data-filter="board">
                <span>Board</span> <i class="fas fa-chevron-down"></i>
                <div class="filter-list board-filter-list"></div>
            </a>
            <a href="#" class="filter-item" data-filter="type">
                <span>Type</span> <i class="fas fa-chevron-down"></i>
                <div class="filter-list">
                    <div class="filter-value-item" data-filter-value="bug">Bug</div>
                    <div class="filter-value-item" data-filter-value="task">Task</div>
                    <div class="filter-value-item" data-filter-value="story">Story</div>
                    <div class="filter-value-item" data-filter-value="epic">Epic</div>
                    <div class="filter-value-item" data-filter-value="theme">Theme</div>
                    <div class="filter-value-item" data-filter-value="clear">No Filter</div>
                </div>
            </a>
            <a href="#" class="filter-item" data-filter="priority">
                <span>Priority</span> <i class="fas fa-chevron-down"></i>
                <div class="filter-list">
                    <div class="filter-value-item" data-filter-value="highest">Highest</div>
                    <div class="filter-value-item" data-filter-value="high">High</div>
                    <div class="filter-value-item" data-filter-value="medium">Medium</div>
                    <div class="filter-value-item" data-filter-value="low">Low</div>
                    <div class="filter-value-item" data-filter-value="lowest">Lowest</div>
                    <div class="filter-value-item" data-filter-value="clear">No Filter</div>
                </div>
            </a>
            <a href="#" class="filter-item" data-filter="status">
                <span>Status</span> <i class="fas fa-chevron-down"></i>
                <div class="filter-list">
                    <div class="filter-value-item" data-filter-value="backlog">Backlog</div>
                    <div class="filter-value-item" data-filter-value="to_do">To Do</div>
                    <div class="filter-value-item" data-filter-value="in_progress">In Progress</div>
                    <div class="filter-value-item" data-filter-value="done">Done</div>
                    <div class="filter-value-item" data-filter-value="clear">No Filter</div>
                </div>
            </a>
            <a href="#" class="filter-item" data-filter="assignee">
                <span>Assignee</span> <i class="fas fa-chevron-down"></i>
                <div class="filter-list assignee-filter-list"></div>
            </a>
            <a href="#" class="clear-filters">Clear Filters</a>
            <div class="list-counter"></div>
        </div>
        @searchbox
        @endsearchbox
    </div>
    <div class="issue-list-container"></div>
    <div class="pagination-container"></div>
    <div class="page-counter"></div>
    @spinner
    @endspinner
</div>
