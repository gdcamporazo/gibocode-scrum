<div class="users-content">
    <h4>Users</h4>
    <div class="content-top-section">
        <div class="users-counter"></div>
        @searchbox
        @endsearchbox
    </div>
    <div class="users-container"></div>
    @spinner
    @endspinner
</div>
