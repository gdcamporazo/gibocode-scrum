<div class="user-content">
    <h4>{{ $user->getFullname() }}</h4>
    <div class="breadcrumbs">
        <a href="{{ route('scrum-users') }}">Users</a> > User
    </div>
    <div class="user-container"></div>
    @spinner
    @endspinner
</div>
