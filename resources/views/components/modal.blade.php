<div class="page-modal-container">
    <div class="page-modal">
        <div class="page-modal-header">
            <div class="page-modal-title"></div>
            <i class="fas fa-times-circle modal-button-close" title="Close"></i>
        </div>
        <div class="page-modal-body">
            <div class="page-modal-icon"></div>
            <div class="page-modal-message"></div>
        </div>
        <div class="page-modal-footer">
            <button type="button" class="btn btn-primary modal-button-ok">OK</button>
            <button type="button" class="btn btn-secondary modal-button-cancel">Cancel</button>
        </div>
        <div class="page-modal-progress-bar-container">
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
        </div>
    </div>
</div>

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('modal.js', 'js') }}"></script>
@endsection
