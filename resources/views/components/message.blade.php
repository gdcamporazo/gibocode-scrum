<div class="alert message">
    <div class="message-icon"></div>
    <div class="message-content"></div>
</div>

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('message.js', 'js') }}"></script>
@endsection
