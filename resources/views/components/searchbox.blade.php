<div class="search search-box">
    <input type="text" class="form-control search-keyword" placeholder="Search"/>
    <button class="btn btn-icon search-button" title="Search">
        <i class="fas fa-search"></i>
    </button>
</div>

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('searchbox.js', 'js') }}"></script>
@endsection
