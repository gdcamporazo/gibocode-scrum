@section('topbar')

    <div class="logo-container">
        <img src="{{ asset_path('banner.jpg', 'images') }}"/>
    </div>

    <div class="current-user-container">
        <a class="btn btn-default">
            <div>Hi, <span id="current-user">{{ auth()->user()->username }}</span>!</div>
            <i class="fas fa-user-circle"></i>
        </a>
    </div>

    <div class="topbar-options">
        <a class="btn btn-default logout-button" href="{{ route('scrum-logout') }}"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </div>

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('topbar.js', 'js') }}"></script>
@endsection
