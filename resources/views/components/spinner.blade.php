<div class="spinner-container">
    <button class="btn" type="button" disabled>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span>Loading...</span>
    </button>
</div>

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('spinner.js', 'js') }}"></script>
@endsection
