@section('sidebar')

    <div class="sidebar-item">
        <a class="btn btn-default {{ (!empty($is_dashboard_selected)) ? 'selected' : '' }}" href="{{ route('scrum-dashboard') }}"><i class="fas fa-tachometer-alt"></i>Dashboard</a>
    </div>
    <div class="sidebar-item">
        <a class="btn btn-default {{ (!empty($is_boards_selected)) ? 'selected' : '' }}" href="{{ route('scrum-boards') }}"><i class="fas fa-clipboard-list"></i>Boards</a>
    </div>
    <div class="sidebar-item">
        <a class="btn btn-default {{ (!empty($is_sprints_selected)) ? 'selected' : '' }}" href="{{ route('scrum-sprints') }}"><i class="fas fa-running"></i>Sprints</a>
    </div>
    <div class="sidebar-item">
        <a class="btn btn-default {{ (!empty($is_backlog_selected)) ? 'selected' : '' }}" href="{{ route('scrum-backlog') }}"><i class="fas fa-boxes"></i>Backlog</a>
    </div>
    <div class="sidebar-item">
        <a class="btn btn-default {{ (!empty($is_issues_selected)) ? 'selected' : '' }}" href="{{ route('scrum-issues') }}"><i class="fas fa-bug"></i>Issues</a>
    </div>
    <div class="sidebar-item">
        <a class="btn btn-default {{ (!empty($is_users_selected)) ? 'selected' : '' }}" href="{{ route('scrum-users') }}"><i class="fas fa-user-friends"></i>Users</a>
    </div>

@endsection
