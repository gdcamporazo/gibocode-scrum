<!DOCTYPE html>
<html>
    <head>
        @section('meta')
            <title>{{ config('scrum-site.name') }} - @yield('title')</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
        @show
        @section('header-css')
            <link type="text/css" rel="stylesheet" href="{{ asset_path('bootstrap.min.css', 'css') }}"/>
            <link type="text/css" rel="stylesheet" href="{{ asset_path('font-quicksand.css', 'css') }}"/>
            <link type="text/css" rel="stylesheet" href="{{ asset_path('font-awesome.css', 'css') }}"/>
            <link type="text/css" rel="stylesheet" href="{{ asset_path('main.css', 'css') }}"/>
        @show
        @section('header-js')
            <script type="text/javascript" src="{{ asset_path('jquery.min.js', 'js') }}"></script>
        @show
        @section('header')
    </head>
    <body>

        @section('topbar')

        <div class="container">
            @yield('container')
        </div>

        @modal
        @endmodal

        @section('footer')
        @section('footer-js')

        @show
    </body>
</html>
