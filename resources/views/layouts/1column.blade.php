@extends('scrum::layouts.default')

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('content.css', 'css') }}"/>
@endsection

@section('container')

    <div class="content one-column-layout-content">
        @yield('content')
    </div>

@endsection
