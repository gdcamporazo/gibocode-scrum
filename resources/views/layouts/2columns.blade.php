@extends('scrum::layouts.1column')

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('topbar.css', 'css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset_path('sidebar.css', 'css') }}"/>
@endsection

@section('container')

    @message
    @endmessage

    <div class="topbar">
        @yield('topbar')
    </div>

    <div class="sidebar">
        @yield('sidebar')
    </div>

    <div class="content two-columns-layout-content">
        @yield('content')
    </div>

    <div class="drawer">

        @yield('drawer')

        <div class="drawer-spinner-container">
            <button class="btn" type="button" disabled>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span>Loading...</span>
            </button>
        </div>

    </div>

@endsection
