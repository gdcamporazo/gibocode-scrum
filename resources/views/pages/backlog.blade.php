@extends('scrum::layouts.2columns')

@section('title', 'Backlog')

@section('header-css')
    @parent

@endsection

@topbar
@endtopbar

@sidebar(['is_backlog_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.backlog.content')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('backlog.js', 'js') }}"></script>
@endsection
