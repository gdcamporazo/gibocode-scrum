@extends('scrum::layouts.1column')

@section('title', 'Register')

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('register.css', 'css') }}"/>
@endsection

@section('content')

    @include('scrum::sections.register.content')

@endsection
