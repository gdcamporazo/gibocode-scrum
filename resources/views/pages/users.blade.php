@extends('scrum::layouts.2columns')

@section('title', 'Users')

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('users.css', 'css') }}"/>
@endsection

@topbar
@endtopbar

@sidebar(['is_users_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.users.content')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('users.js', 'js') }}"></script>
@endsection
