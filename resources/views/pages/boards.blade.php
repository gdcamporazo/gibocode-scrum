@extends('scrum::layouts.2columns')

@section('title', 'Boards')

@section('header-css')
    @parent

@endsection

@topbar
@endtopbar

@sidebar(['is_boards_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.boards.content')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('boards.js', 'js') }}"></script>
@endsection
