@extends('scrum::layouts.2columns')

@section('title', 'All Issues')

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('issues.css', 'css') }}"/>
@endsection

@topbar
@endtopbar

@sidebar(['is_issues_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.issues.content')

@endsection

@section('drawer')

    @include('scrum::sections.issues.drawer')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('issues.js', 'js') }}"></script>
@endsection
