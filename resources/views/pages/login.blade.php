@extends('scrum::layouts.1column')

@section('title', 'Login')

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('login.css', 'css') }}"/>
@endsection

@section('content')

    @include('scrum::sections.login.content')

@endsection
