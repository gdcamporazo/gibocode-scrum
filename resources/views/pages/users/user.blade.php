@extends('scrum::layouts.2columns')

@section('title', 'User - ' . $user->getFullname())

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('user.css', 'css') }}"/>
@endsection

@topbar
@endtopbar

@sidebar(['is_users_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.users.user.content')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('user.js', 'js') }}"></script>
@endsection
