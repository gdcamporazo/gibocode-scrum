@extends('scrum::layouts.1column')

@section('title', $data['title'])

@section('header-css')
    @parent
    <link type="text/css" rel="stylesheet" href="{{ asset_path('error.css', 'css') }}"/>
@endsection

@section('content')

    @include('scrum::sections.error.content')

@endsection
