@extends('scrum::layouts.2columns')

@section('title', 'Sprints')

@section('header-css')
    @parent

@endsection

@topbar
@endtopbar

@sidebar(['is_sprints_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.sprints.content')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('sprints.js', 'js') }}"></script>
@endsection
