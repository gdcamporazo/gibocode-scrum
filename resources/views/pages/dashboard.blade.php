@extends('scrum::layouts.2columns')

@section('title', 'Dashboard')

@topbar
@endtopbar

@sidebar(['is_dashboard_selected' => true])
@endsidebar

@section('content')

    @include('scrum::sections.dashboard.content')

@endsection

@section('footer-js')
    @parent
    <script type="text/javascript" src="{{ asset_path('dashboard.js', 'js') }}"></script>
@endsection
