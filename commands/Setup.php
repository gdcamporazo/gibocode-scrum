<?php

/**
 * Setup Class
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Commands;

use Illuminate\Foundation\Console\ClosureCommand;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class Setup {

    /**
     * Installs tables
     * @param Illuminate\Foundation\Console\ClosureCommand $command
     * @return void
     */
    public static function install(ClosureCommand $command) {

        $command->info('Creating scrum users database...');

        Schema::create('scrum_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        $command->comment('Done setup.');
    }

    /**
     * Uninstalls tables
     * @param Illuminate\Foundation\Console\ClosureCommand $command
     * @return void
     */
    public static function uninstall(ClosureCommand $command) {

        if ($command->confirm('Are you sure you want to uninstall?')) {

            $command->info('Removing scrum users database...');

            Schema::dropIfExists('scrum_users');

            $command->comment('Done.');
        }
    }
}
