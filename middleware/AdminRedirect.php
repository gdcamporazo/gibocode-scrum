<?php

/**
 * Admin Redirect Middleware
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminRedirect {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

        config(['auth.defaults.guard' => 'scrum_users']);   // Overrides the default user authentication

        if (Auth::guard($guard)->check()) {

            return redirect()->route('scrum-dashboard');
        }

        return $next($request);
    }
}
