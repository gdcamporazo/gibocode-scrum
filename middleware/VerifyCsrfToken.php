<?php

/**
 * Verify CSRF Token Middleware
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Encryption\Encrypter;

class VerifyCsrfToken extends \App\Http\Middleware\VerifyCsrfToken {

    /**
     * @var array $except
     */
    protected $except = [
        'issues/*',
        'users/*'
    ];

    /**
     * Verify CSRF Token Constructor
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Contracts\Encryption\Encrypter  $encrypter
     * @return void
     */
    public function __construct(Application $app, Encrypter $encrypter) {

        foreach ($this->except as $index => $except) {

            $this->except[$index] = config('scrum-site.base_path') . $except;
        }

        parent::__construct($app,  $encrypter);
    }
}
