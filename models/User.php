<?php

/**
 * User Model
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Models;

class User extends \Illuminate\Foundation\Auth\User {

    protected $table = 'scrum_users';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'username',
        'password',
    ];

    /**
     * @var array $hidden
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * Gets the user's ID
     * @return integer
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Gets the user's firstname
     * @return string
     */
    public function getFirstname() {

        return $this->firstname;
    }

    /**
     * Gets the user's lastname
     * @return string
     */
    public function getLastname() {

        return $this->lastname;
    }

    /**
     * Gets the user's fullname
     * @return string
     */
    public function getFullname() {

        return trim($this->getFirstname() . ' ' . $this->getLastname());
    }

    /**
     * Gets the user's email address
     * @return string
     */
    public function getEmail() {

        return $this->email;
    }

    /**
     * Gets the user's username
     * @return string
     */
    public function getUsername() {

        return $this->username;
    }

    /**
     * Checks if the user is the current user
     * @return boolean
     */
    public function isCurrentUser() {

        return $this->getId() == auth()->user()->id;
    }
}
