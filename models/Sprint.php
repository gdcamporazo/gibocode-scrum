<?php

/**
 * Sprint Model
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Models;

class Sprint extends Base {

    /**
     * @var string $collection
     */
    protected $collection = 'sprints';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'board_id',
        'created_by',
        'title',
        'status' // contains values 'open', 'closed'
    ];
}
