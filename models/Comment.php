<?php

/**
 * Comment Model
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Models;

class Comment extends Base {

    /**
     * @var string $collection
     */
    protected $collection = 'comments';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'issue_id',
        'author',
        'content'
    ];
}
