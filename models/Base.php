<?php

/**
 * Base Mongo Model
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Base extends Eloquent {

    /**
     * @var string $connection
     */
    protected $connection = 'scrum_mongodb';

    /**
     * @var string $database
     */
    protected $database = 'scrum';
}
