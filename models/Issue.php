<?php

/**
 * Issue Model
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Models;

use App;

class Issue extends Base {

    /**
     * @var string $collection
     */
    protected $collection = 'issues';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'issue_id',
        'board',
        'sprint',
        'title',
        'description',
        'priority',
        'type',
        'points',
        'status',
        'assignee',
        'reporter',
        'date_added',
        'date_completed',
        'created_by',
        'locked',
        'archived'
    ];

    /**
     * @var array $attributes
     */
    protected $attributes = [
        'issue_id' => '',
        'board' => '',
        'sprint' => '',
        'title' => '',
        'description' => '',
        'priority' => '',
        'type' => '',
        'points' => null,
        'status' => '',
        'assignee' => [],
        'reporter' => null,
        'date_added' => null,
        'date_completed' => null,
        'created_by' => null,
        'locked' => false,
        'archived' => false
    ];

    /**
     * Gets the issue ID
     * @return string
     */
    public function getIssueId() {

        return $this->issue_id;
    }

    /**
     * Gets the board where the issue is assigned to
     * @return string
     */
    public function getBoard() {

        return $this->board;
    }

    /**
     * Gets the issue title (with a limit of 50 characters)
     * @return string
     */
    public function getTitle() {

        return substr($this->title, 0, 50);
    }

    /**
     * Gets issue priority
     * @return string
     */
    public function getPriority() {

        $priorities = ['highest', 'high', 'medium', 'low', 'lowest'];

        return (in_array($priority = $this->priority, $priorities)) ? $priority : '';
    }

    /**
     * Gets the type of issue
     * @return string
     */
    public function getType() {

        $types = ['bug', 'task', 'story', 'epic', 'theme'];

        return (in_array($type = $this->type, $types)) ? $type : '';
    }

    /**
     * Gets the status of the issue
     * @return string
     */
    public function getStatus() {

        $statuses = ['backlog', 'to_do', 'in_progress', 'done'];

        return (in_array($status = $this->status, $statuses)) ? $status : '';
    }

    /**
     * Gets the list of assignee by user IDs to whom the issue was assigned to
     * @return array
     */
    public function getAssignee() {

        return $this->assignee;
    }

    /**
     * Gets the list of assignee by their user name to whom the issue was assigned to
     * @return array
     */
    public function getAssigneeByUsername() {

        $assignee = [];

        foreach ($this->getAssignee() as $id) {

            $assignee[] = App::make('User')->where('id', $id)->first()->getUsername();
        }

        return $assignee;
    }
}
