<?php

/**
 * Board Model
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Models;

class Board extends Base {

    /**
     * @var string $collection
     */
    protected $collection = 'boards';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'board_id',
        'created_by',
        'name',
        'issues_accumulated',
        'locked',
        'archived'
    ];

    /**
     * Gets the board ID
     * @return string
     */
    public function getBoardId() {

        return $this->board_id;
    }

    /**
     * Gets the name of the board
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Gets the number of issues assigned to this board accumutively
     * @return int
     */
    public function getIssuesAccumulated() {

        return $this->issues_accumulated;
    }
}
