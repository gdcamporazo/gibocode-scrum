<?php

/**
 * Scrum Service Provider
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Blade;

class ScrumServiceProvider extends ServiceProvider {

    /**
     * Register any application services
     * @return void
     */
    public function register() {

        $this->loadConfigs();
        $this->loadHelpers();

        $classes = array_keys($this->loadClasses()->getClassMap());

        $this->bindClasses($classes);
    }

    /**
     * Bootstrap any application services
     * @param Illuminate\Routing\Router
     * @return void
     */
    public function boot(Router $router) {

        $this->loadDBConnections();
        $this->loadAuth();
        $this->loadMiddleware($router);
        $this->loadRoutes();
        $this->loadViews();
        $this->loadComponents();
    }

    /**
     * Gets the packages path
     * @return string
     */
    protected function getPackagesPath() {

        $dir = dirname(__DIR__);

        if (!empty($packages_path = config('scrum-packages.path'))) {

            $tmp = str_replace('vendor', $packages_path, $dir);

            if (file_exists("{$tmp}/providers/ScrumServiceProvider.php")) {

                $dir = $tmp;

                $this->loadConfigs($dir); // Remerging config if the package is in working environment
            }
        }

        return $dir;
    }

    /**
     * Loads configurations
     * @param string $dir
     * @return void
     */
    protected function loadConfigs($dir = '') {

        $dir = (empty($dir)) ? dirname(__DIR__) : $dir;

        foreach (scandir("{$dir}/config") as $config) {

            if (pathinfo($config, PATHINFO_EXTENSION) == 'php') {

                $this->mergeConfigFrom("{$dir}/config/{$config}", 'scrum-' . str_replace('.php', '', $config));
            }
        }
    }

    /**
     * Loads helpers
     * @return void
     */
    protected function loadHelpers() {

        $dir = $this->getPackagesPath();

        foreach (scandir("{$dir}/helpers") as $helper) {

            if (pathinfo($helper, PATHINFO_EXTENSION) == 'php') {

                require_once "{$dir}/helpers/{$helper}";
            }
        }
    }

    /**
     * Loads classes
     * @return array
     */
    protected function loadClasses() {

        $dir = base_path();
        $file = 'vendor/autoload.php';
        $path = $this->getPackagesPath();

        if (!file_exists($path = "{$path}/{$file}")) {

            $path = "{$dir}/{$file}";
        }

        return require $path;
    }

    /**
     * Binds classes to the Laravel application
     * @param array $classes
     * @return void
     */
    protected function bindClasses($classes) {

        foreach ($classes as $class) {

            if (starts_with($class, 'Gibocode\Scrum')) {

                $class_name = substr($class, strrpos($class, '\\') + 1);
                $this->app->bind($class_name, $class);
            }
        }
    }

    /**
     * Loads the database connections
     * @return void
     */
    protected function loadDBConnections() {

        $connections = config('scrum-database.connections');

        foreach ($connections as $name => $value) {

            config(["database.connections.{$name}" => $value]);
        }
    }

    /**
     * Loads the authentication config
     * @return void
     */
    protected function loadAuth() {

        $auths = config('scrum-auth');

        foreach ($auths as $type => $auth) {

            foreach ($auth as $name => $value) {

                config(["auth.{$type}.{$name}" => $value]);
            }
        }
    }

    /**
     * Loads all middleware
     * @param Illuminate\Routing\Router
     * @return void
     */
    protected function loadMiddleware(Router $router) {

        $methodTypes = [
            'route'     =>  'aliasMiddleware',
            'groups'    =>  'middlewareGroup'
        ];

        $middlewares = config('scrum-middleware');

        if ($middlewares) {

            foreach ($methodTypes as $type => $method) {

                if (isset($middlewares[$type]) && !empty($middleware = $middlewares[$type])) {

                    foreach ($middleware as $alias => $class) {

                        $router->$method($alias, $class);
                    }
                }
            }
        }
    }

    /**
     * Loads routes
     * @return void
     */
    protected function loadRoutes() {

        $dir = $this->getPackagesPath();

        foreach (scandir("{$dir}/routes") as $route) {

            if (pathinfo($route, PATHINFO_EXTENSION) == 'php') {

                $this->loadRoutesFrom("{$dir}/routes/{$route}");
            }
        }
    }

    /**
     * Loads views
     * @return void
     */
    protected function loadViews() {

        $dir = $this->getPackagesPath();

        $this->loadViewsFrom("{$dir}/resources/views", 'scrum');
    }

    /**
     * Loads blade components
     * @return void
     */
    protected function loadComponents() {

        $components = ['message', 'topbar', 'sidebar', 'spinner', 'searchbox', 'modal'];

        foreach ($components as $component) {

            Blade::component("scrum::components.{$component}", $component);
        }
    }
}
