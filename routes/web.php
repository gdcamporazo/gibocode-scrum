<?php

/**
 * Web Routes
 * @author Gilbor Camporazo Jr.
 */

$base_path = config('scrum-site.base_path');

Route::get($base_path . 'assets/{path}/{file}', 'Gibocode\Scrum\Controllers\AssetsController@get');

Route::group(['middleware' => ['scrum.web']], function() use ($base_path) {

    Route::get($base_path, function() {

        return redirect()->route('scrum-login');
    });

    Route::get($base_path . 'login', 'Gibocode\Scrum\Controllers\LoginController@index')->name('scrum-login');
    Route::post($base_path . 'login', 'Gibocode\Scrum\Controllers\LoginController@login');
    Route::get($base_path . 'logout', 'Gibocode\Scrum\Controllers\LoginController@logout')->name('scrum-logout');

    Route::get($base_path . 'register', 'Gibocode\Scrum\Controllers\RegisterController@index')->name('scrum-register');
    Route::post($base_path . 'register', 'Gibocode\Scrum\Controllers\RegisterController@register');

    Route::get($base_path . 'dashboard', 'Gibocode\Scrum\Controllers\DashboardController@index')->name('scrum-dashboard');

    Route::get($base_path . 'boards', 'Gibocode\Scrum\Controllers\BoardsController@index')->name('scrum-boards');

    Route::get($base_path . 'sprints', 'Gibocode\Scrum\Controllers\SprintsController@index')->name('scrum-sprints');

    Route::get($base_path . 'backlog', 'Gibocode\Scrum\Controllers\BacklogController@index')->name('scrum-backlog');

    Route::get($base_path . 'issues', 'Gibocode\Scrum\Controllers\IssuesController@index')->name('scrum-issues');
    Route::post($base_path . 'issues/boards', 'Gibocode\Scrum\Controllers\IssuesController@getBoards');
    Route::post($base_path . 'issues/assignee', 'Gibocode\Scrum\Controllers\IssuesController@getAssignee');
    Route::post($base_path . 'issues/list', 'Gibocode\Scrum\Controllers\IssuesController@getList');
    Route::post($base_path . 'issues/display', 'Gibocode\Scrum\Controllers\IssuesController@getIssue');
    Route::post($base_path . 'issues/save', 'Gibocode\Scrum\Controllers\IssuesController@saveIssue');
    Route::post($base_path . 'issues/comment/submit', 'Gibocode\Scrum\Controllers\IssuesController@submitComment');
    Route::post($base_path . 'issues/comment/delete', 'Gibocode\Scrum\Controllers\IssuesController@deleteComment');

    Route::get($base_path . 'users', 'Gibocode\Scrum\Controllers\UsersController@index')->name('scrum-users');
    Route::post($base_path . 'users/list', 'Gibocode\Scrum\Controllers\UsersController@getList');
    Route::get($base_path . 'users/user/id/{id}', 'Gibocode\Scrum\Controllers\UsersController@viewUser');
});

Route::any($base_path . '{page?}', 'Gibocode\Scrum\Controllers\ErrorController@view')->where('page', '.*');
