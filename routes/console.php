<?php

/**
 * Console Routes
 * @author Gilbor Camporazo Jr.
 */

Artisan::command('scrum:setup:install', function() {
    \Gibocode\Scrum\Commands\Setup::install($this);
})->describe('Installs scrum database');

Artisan::command('scrum:setup:uninstall', function() {
    \Gibocode\Scrum\Commands\Setup::uninstall($this);
})->describe('Uninstalls scrum database');
