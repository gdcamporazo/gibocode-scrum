<?php

return [
    'content_types' => [
        'css' =>  ['css' => 'text/css'],
        'js'  =>  ['js'  => 'text/javascript'],
        'images' =>  [
            'jpg' =>  'image/jpeg'
        ],
        'fonts' =>  [
            'woff2' => 'font/woff2'
        ],
        'webfonts' => [
            'eot'   =>  'application/vnd.ms-fontobject',
            'svg'   =>  'image/svg+xml',
            'ttf'   =>  'application/font-sfnt',
            'woff'  =>  'application/font-woff',
            'woff2' =>  'font/woff2'
        ]
    ]
];
