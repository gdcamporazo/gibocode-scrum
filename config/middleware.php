<?php

return [
    'route' =>  [
        'scrum.auth'    =>  \Gibocode\Scrum\Middleware\Authenticate::class,
        'scrum.admin'   =>  \Gibocode\Scrum\Middleware\AdminRedirect::class,
        'scrum.csrf'    =>  \Gibocode\Scrum\Middleware\VerifyCsrfToken::class
    ],
    'groups' =>  [
        'scrum.web' =>  [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            'scrum.csrf',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ]
    ]
];
