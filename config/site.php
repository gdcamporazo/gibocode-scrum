<?php

return [
    'name'              =>  env('SCRUM_SITE_NAME', 'Gibocode Scrum'),
    'base_path'         =>  env('SCRUM_BASE_PATH', 'scrum') . '/',
    'registration_code' =>  env('SCRUM_REGISTRATION_CODE', 'gibocode_scrum_1234')
];
