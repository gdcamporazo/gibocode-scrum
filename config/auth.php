<?php

return [
    'guards'  =>  [
        'scrum_users' => [
            'driver'    => 'session',
            'provider'  => 'scrum_users'
        ]
    ],
    'providers' => [
        'scrum_users' => [
            'driver'    => 'eloquent',
            'model'     => \Gibocode\Scrum\Models\User::class
        ]
    ]
];
