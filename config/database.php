<?php

return [
    'connections' => [
        'scrum_mongodb'   => [
            'driver'    => 'mongodb',
            'host'      => env('SCRUM_MONGODB_HOST', 'localhost'),
            'port'      => 27017,
            'database'  => 'scrum'
        ]
    ]
];
