<?php

/**
 * Issue File Manager
 * @author Gilbor Camporazo Jr.
 */

namespace Gibocode\Scrum\Lib;

use App;
use Exception;

class IssueFileManager {

    public function import($file) {

        if (!ends_with(strtolower($file), '.csv')) throw new Exception('Invalid CSV format.');
        if (!file_exists($filepath = dirname(__DIR__) . '/public/files/uploads/' . $file)) throw new Exception('File does not exists.');
        if (empty($contents = file_get_contents($filepath))) throw new Exception('File is empty.');

        $issues = [];
        $count = 0;

        foreach (explode("\r\n", $contents) as $index => $line) {

            if ($index == 0) continue;  // Excludes header

            $values = explode(',', $line);

            for ($valueindex = 0; $valueindex < ($fieldcount = count($values)); $valueindex++) {

                $value = $values[$valueindex];

                if ($valueindex == 0 && empty($value)) break;

                if ($valueindex == 2) {

                    if ($fieldcount > 11) {

                        while ($valueindex < (2 + $fieldcount - 11)) {

                            $valueindex++;
                            $values[$valueindex];
                        }
                    }
                }

                $issues[$count][] = $value;
            }

            $count++;
        }

        if (!empty($issues)) {

            $fields = [
                'board',
                'title',
                'description',
                'priority',
                'type',
                'points',
                'status',
                'assignee',
                'reporter',
                'date_added',
                'date_completed'
            ];

            foreach ($issues as $issue) {

                foreach ($fields as $index => $field) {

                    $value = $issue[$index];

                    if (in_array($index, [0, 3, 4, 6])) $value = strtolower($value);

                    if ($index == 0) {

                        $value = str_replace(' ', '', $value);

                        $board = App::make('Board')->where('board_id', $value)->first();
                        $board->increment('issues_accumulated', 1);
                        $issues_accumulated = $board->getIssuesAccumulated();
                        $issue_id = strtoupper($value) . '-' . $issues_accumulated;
                    }

                    if ($index == 5) $value = (int)$value;
                    if ($index == 6) $value = str_replace(' ', '_', $value);

                    if (empty($value)) {

                        if ($index == 1) $value = '(Untitled)';
                        if ($index == 3) $value = 'lowest';
                        if ($index == 4) $value = 'task';
                        if ($index == 6) $value = 'backlog';
                    }

                    if ($index == 7) {

                        if ($value == 'Unassigned' || empty($value)) $value = [];
                        else {

                            $username = strtolower($value);
                            $user = App::make('User')->where('username', $username)->first();
                            $value = [$user->getId()];
                        }
                    }

                    if (in_array((int)$index, [9, 10])) {

                        $value = (!empty($value)) ? new \MongoDB\BSON\UTCDateTime(strtotime($value) * 1000) : null;
                    }

                    $data[$fields[$index]] = $value;

                    if ($index == 10) break;
                }

                $data['issue_id'] = $issue_id;
                $data['created_by'] = auth()->user()->id;

                App::make('Issue')->create($data);
            }
        }

        return count($issues);
    }
}
