<?php

function content_type($type, $file) {

    $contentTypes = config('scrum-files.content_types');

    return $contentTypes[$type][pathinfo($file, PATHINFO_EXTENSION)];
}

function registration_code() {

    return config('scrum-site.registration_code');
}

function asset_path($file, $type) {

    $path = url('/') . '/' . config('scrum-site.base_path') . "assets/{$type}/{$file}";

    return $path;
}
