var messageTimeout;

$(document).ready(function() {

    showMessage('warning', 'No content.');
});

function showMessage(type, message, timeout = false) {

    refreshMessage();
    setMessageType(type);

    $('.message > .message-content').html(message);
    $('.message').css('left', ($('body').width() / 2) - ($('.message').width() / 2));

    $('.message').show().animate({
        top: '35px',
        opacity: 1
    }, function() {

        if (timeout) {

            fadeOutMessage();
        }
    });
}

function hideMessage() {

    if ($('.message').is(':visible')) {

        $('.message').stop().hide();
    }
}

function refreshMessage() {

    // Stops the animation to display a new message
    if ($('.message').is(':visible')) {

        clearTimeout(messageTimeout);
        hideMessage();
    }

    // Sets the position and opacity to default
    $('.message').css({
        top: 0,
        opacity: 0
    });
}

function setMessageType(type) {

    var icon_types = {
        'success'    :   [
            'alert-success',
            'fas fa-check-circle'
        ],
        'warning'   :   [
            'alert-warning',
            'fas fa-exclamation-circle'
        ],
        'error'     :   [
            'alert-danger',
            'fas fa-times-circle'
        ]
    };

    $.each(icon_types, function(key, icon_type) {

        $('.message').removeClass(icon_type[0]);
    });

    $('.message').addClass(icon_types[type][0]);
    $('.message > .message-icon').html('<i class="' + icon_types[type][1] + '"></i>');
}

function fadeOutMessage() {

    messageTimeout = setTimeout(function() {

        $('.message').fadeOut(1000);

    }, 5000);
}
