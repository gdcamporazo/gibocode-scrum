$(document).ready(function() {

    $(window).click(function(e) {

        if ($(e.target).closest('.current-user-container').length && !$('.topbar-options').is(':visible')) {

            $('.topbar-options').show();
        }
        else if (!$(e.target).closest('.topbar-options').length) {

            $('.topbar-options').hide();
        }
    });
});
