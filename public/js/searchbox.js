var filter = {};

var search_action = function () {

    showMessage('error', 'You are trying to search \'' + filter.keyword + '\' but no search action was set.', true);
};

$(document).ready(function() {

    $('.search-button').click(function() {

        var keyword = $('.search-keyword').val();
        filter.keyword = keyword;

        if (keyword.trim() == '') delete filter.keyword;

        search_action();
    });

    $('.search-keyword').keypress(function(e) {

        if (e.keyCode == 13) $('.search-button').trigger('click');
    });
});

function setSearchAction(action) {

    search_action = action;
}
