var modal_action = null;

$(document).ready(function() {

    $('.modal-button-close, .modal-button-cancel').click(function() {

        hideModal();
    });

    $('.page-modal-footer > .btn').click(function() {

        var result = $(this).hasClass('modal-button-ok');

        if (!result) hideModal();

        modal_action(result);
    });
});

function showModal(type, title, message, action) {

    hideProgress();
    setModalType(type);
    setModalTitle(title);
    setModalMessage(message);

    modal_action = action;

    $('body').css('overflow-y', 'hidden');
    $('.page-modal-container').fadeIn(200);
}

function setModalType(type) {

    var modal_types = {
        'question'  :   [
            'modal-question',
            'fas fa-question-circle'
        ]
    };

    $.each(modal_types, function(key, modal_type) {

        $('.page-modal').removeClass(modal_type[0])
    });

    $('.page-modal').addClass(modal_types[type][0]);
    $('.page-modal-icon').html('<i class="' + modal_types[type][1] + '"></i>');
}

function setModalTitle(title) {

    $('.page-modal-title').text(title);
}

function setModalMessage(message) {

    $('.page-modal-message').html(message);
}

function hideModal() {

    $('.page-modal-container').fadeOut(150, function() {

        $('body').css('overflow-y', 'auto');
    });
}

function showProgress(title) {

    setModalTitle(title);

    $('.page-modal-body').hide();
    $('.page-modal-footer').hide();
    $('.page-modal-progress-bar-container').show();
}

function hideProgress() {

    $('.page-modal-progress-bar-container').hide();
    $('.page-modal-body').show();
    $('.page-modal-footer').show();
}
