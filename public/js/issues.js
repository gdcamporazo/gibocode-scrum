var current_url = window.location.href;
var filter = {};
var current_page = 1;

$(document).ready(function() {

    hideMessage();
    showBoards();
    showAssignee();
    showList(filter);

    setSearchAction(function() {

        current_page = 1;   // Resets the pagination

        showList(filter);
    });

    $(window).click(function(e) {

        $('.filter-list').hide();

        // Shows the filter list
        if (typeof($(e.target).parent().data('filter')) !== 'undefined') {

            $(e.target).parent().find('.filter-list').show();
        }
        else if (typeof($(e.target).data('filter')) !== 'undefined') {

            $(e.target).find('.filter-list').show();
        }

        // Closes the issue type list in the issue drawer
        if ($('.issue-type-list').is(':visible') &&
            $(e.target).closest('.issue-type-box-container, .issue-type-selected-item').length == 0
        ) {

            $('.issue-type-list').slideUp(100);
        }

        // Closes the issue priority list in the issue drawer
        if ($('.issue-priority-list').is(':visible') &&
            $(e.target).closest('.issue-priority-box-container, .issue-priority-selected-item').length == 0
        ) {

            $('.issue-priority-list').slideUp(100);
        }

        // Hides the drawer
        if ($(e.target).closest('.issue-item').length == 0 &&
            $(e.target).closest('.drawer').length == 0 &&
            !$('.page-modal-container').is(':visible')
        ) {

            hideDrawer();
        }
    });

    // Filter item selected
    $('.filter-list > div').click(function() {

        applyFilter($(this));
    });

    // Clear all the filters
    $('.clear-filters').click(function() {

        var keyword = $('.search-keyword').val();

        $.each($('.filter-item'), function(index, elem) {

            var text = formatText($(elem).data('filter'));
            $(elem).children().first().text(text).removeClass('filter-applied');
        });

        filter = {};

        if (keyword.trim() != '') filter.keyword = keyword;

        $(this).hide();

        current_page = 1;   // Resets the pagination

        showList(filter);
    });

    $('.issue-title').keyup(function() {

        $('.issue-title-counter').html($(this).val().length + '/50');

        if ($(this).val().length == 50) {

            showMessage('error', 'Issue title already reached the maximum limit of characters.', true);
        }
        else hideMessage();
    })

    $('.issue-type-box-container, .issue-type-selected-item, .issue-priority-box-container, .issue-priority-selected-item').click(function() {

        $(this).parent().children().last().slideToggle(100);
    });

    $('.issue-type-item').click(function() {

        $('.issue-type').val($(this).data('issueType'));
        $('.issue-type-selected-item').html($(this).html());
    });

    $('.issue-priority-item').click(function() {

        $('.issue-priority').val($(this).data('issuePriority'));
        $('.issue-priority-selected-item').html($(this).html());
    });

    $('.issue-save-button').click(function() {

        saveIssue();
    });

    $('.issue-cancel-button').click(function() {

        hideDrawer();
    });

    $('.new-comment').keyup(function() {

        if (!$('.submit-comment').hasClass('disabled')) $('.submit-comment').addClass('disabled');
        if ($(this).val().trim() !== '' && $('.submit-comment').hasClass('disabled')) $('.submit-comment').removeClass('disabled');
    });

    $('.submit-comment').click(function() {

        submitComment();
    });
});

function showBoards() {

    $.post(current_url + '/boards', function(data, status, xhr) {

        var boards = '';

        if (status == 'success') {

            $.each(data, function(index, board) {

                if (isNaN(index)) return true;

                boards += '<div class="filter-value-item" data-filter-value="' + board.board_id + '">' + board.name + '</div>';
            });

            if (typeof (data.error) !== 'undefined') {

                showMessage('error', data.error);
            }
        }

        boards += '<div class="filter-value-item" data-filter-value="clear">No Filter</div>';
        boards += '<script type="text\/javascript">$(\'.filter-value-item\').click(function() { applyFilter($(this)); });<\/script>';

        $('.board-filter-list').html(boards);

    }, 'json');
}

function showAssignee() {

    $.post(current_url + '/assignee', function(data, status, xhr) {

        var assignees = '<div class="filter-value-item" data-filter-value="">Unassigned</div>';

        if (status == 'success') {

            $.each(data, function(index, assignee) {

                if (isNaN(index)) return true;

                assignees += '<div class="filter-value-item" data-filter-value="' + assignee.id + '">' + assignee.username + '</div>';
            });

            if (typeof (data.error) !== 'undefined') {

                showMessage('error', data.error);
            }
        }

        assignees += '<div class="filter-value-item" data-filter-value="clear">No Filter</div>';
        assignees += '<script type="text\/javascript">$(\'.filter-value-item\').click(function() { applyFilter($(this)); });<\/script>';

        $('.assignee-filter-list').html(assignees);

    }, 'json');
}

function showList(filter = {}) {

    $('.list-counter').hide();
    $('.issue-list-container').hide();
    $('.pagination-container').hide();
    $('.page-counter').hide();

    showSpinner();

    filter.page = current_page;

    $.post(current_url + '/list', filter, function(data, status, xhr) {

        if (status == 'success') {

            var counter = '';
            var issues = '';
            var count = 0;
            var pagination = '';
            var page_counter = '';

            $.each(data, function(index, item) {

                if (isNaN(index)) return true;

                issues += '<div class="issue-item" data-issue-id="' +  item.issue_id + '">';

                issues += '<div class="issue-item-top-section">';
                issues += '<div class="issue-item-id">' + getIssueTypeIcon(item.type) + item.issue_id + '</div>';
                issues += '<div class="issue-item-status ' + getStatusBadge(item.status) + '">' + formatText(item.status) + '</div>';
                issues += '<div style="clear:both"></div>'
                issues += '</div>';

                issues += '<div class="issue-item-bottom-section">';
                issues += '<div class="issue-item-title">' + getPriorityIcon(item.priority) + item.title + '</div>';
                issues += '<div class="issue-item-assignee">' + getAssignee(item.assignee) + '</div>';
                issues += '<div style="clear:both"></div>'
                issues += '</div>';

                issues += '</div>';

                count++;
            });

            if (typeof (data.error) !== 'undefined') {

                showMessage('error', data.error);
            }

            if (issues == '') issues = '<span style="color:#e06666;position:relative;top:10px;">No items found.</span>';
            else issues += '<script type="text\/javascript">$(\'.issue-item\').click(function() { viewIssue($(this).data("issueId")); });<\/script>';

            if (count > 0) {

                counter = count + ' of ' +  data.total_issues + ' Issues';
                pagination = getPagination(data.page, data.total_pages);
                page_counter = 'Page ' + data.page + ' of ' + data.total_pages + ' Pages';
            }

            hideSpinner();

            $('.list-counter').html(counter).fadeIn();
            $('.issue-list-container').html(issues).fadeIn();
            $('.pagination-container').html(pagination).fadeIn();
            $('.page-counter').html(page_counter).fadeIn();
        }

    }, 'json');
}

function getIssueTypeIcon(type) {

    var icons = {
        'bug'   :   'fas fa-egg type-bug',
        'task'  :   'fas fas fa-flag type-task',
        'story' :   'fas fa-bookmark type-story',
        'epic'  :   'fas fa-star type-epic',
        'theme' :   'fas fa-heart type-theme'
    };

    return '<i class="issue-type-icon ' + icons[type] + '" title="' + formatText(type) + '"/></i>';
}

function getPriorityIcon(priority) {

    var icons = {
        'highest'   :   'fas fa-arrow-up priority-highest',
        'high'      :   'fas fa-arrow-up priority-high',
        'medium'    :   'fas fa-minus priority-medium',
        'low'       :   'fas fa-arrow-down priority-low',
        'lowest'    :   'fas fa-arrow-down priority-lowest',
    }

    return '<i class="priority-icon ' + icons[priority] + '" title="' + formatText(priority) + '"/></i>';
}

function getStatusBadge(status) {

    var badges = {
        'backlog'       :   'badge badge-backlog',
        'to_do'         :   'badge badge-todo',
        'in_progress'   :   'badge badge-inprogress',
        'done'          :   'badge badge-done'
    };

    return badges[status];
}

function getAssignee(assignee) {

    var defaultAssignee = '<span style="color:#e06666;">Unassigned</span>';

    if (typeof (assignee) !== 'undefined' && assignee.length > 0) defaultAssignee = assignee.join(', ');

    return defaultAssignee;
}

function formatText(text) {

    if (typeof (text) === 'undefined' || text == null) {

        return '';
    }

    text = text.replace(/_/g, ' ').replace(/\b[a-z]/g, function(char) {

        return char.toUpperCase();
    });

    return text;
}

function applyFilter(elem) {

    var parent = elem.parent().parent();
    var text = elem.text();
    var filter_field = parent.data('filter');
    var filter_value = elem.data('filter-value');

    filter[filter_field] = filter_value
    parent.children().first().addClass('filter-applied');

    if (filter_value == 'clear') {

        text = formatText(filter_field);
        delete filter[filter_field];

        parent.children().first().removeClass('filter-applied');
    }

    parent.children().first().text(text);
    $('.filter-list').hide();

    if (Object.keys(filter).length > 1) $('.clear-filters').show();
    else $('.clear-filters').hide();

    current_page = 1;   // Resets the pagination

    showList(filter);
}

function getPagination(current, pages) {

    var pagination = '<ul class="pagination">';
    pagination += '<li class="pagination-item ' + ((current == 1) ? 'disabled' : '') + '" data-page="previous">Previous</li>';

    for (var i = 1; i <= pages; i++) {

        pagination += '<li class="pagination-item ' + ((current == i) ? 'current-page' : '') + '" data-page="' + i + '">' + i + '</li>';
    }

    pagination += '<li class="pagination-item ' + ((current == pages) ? 'disabled' : '') + '" data-page="next">Next</li>';
    pagination += '</ul>';

    pagination += '<script type="text\/javascript">$(\'.pagination-item\').click(function() { displayPage($(this)); });<\/script>';

    return pagination;
}

function displayPage(elem) {

    var page = elem.data('page');

    if (!elem.hasClass('disabled') && page != current_page) {

        if (page == 'previous') current_page--;
        else if (page == 'next') current_page++;
        else current_page = page;

        filter.page = current_page;

        showList(filter);
    }
}

function viewIssue(issue_id, refreshData = false) {

    var callback = function() {

        $.post(current_url + '/display', {issue_id : issue_id}, function(data, status, xhr) {

            if (status == 'success') {

                if (typeof (data.error) !== 'undefined') {

                    showMessage('error', data.error);
                    return false;
                }

                var title = data.issue.title.substring(0, 50);

                $('.issue-id').html(data.issue.issue_id);
                $('.issue-title-counter').html(title.length + '/50');
                $('.issue-title').val(title);
                $('.issue-description').val(data.issue.description);
                $('.issue-type').val(data.issue.type);
                $('.issue-type-selected-item').html(getIssueTypeIcon(data.issue.type) + formatText(data.issue.type));
                $('.issue-priority').val(data.issue.priority);
                $('.issue-priority-selected-item').html(getPriorityIcon(data.issue.priority) + formatText(data.issue.priority));

                var comments = '';
                var comment_count = 0;

                $.each(data.comments, function(index, comment) {

                    comments += '<div class="comment-item">';

                    comments += '<div class="comment-item-head">';
                    comments += '<div class="comment-item-author">' + comment.author + '</div>';
                    comments += '<div class="comment-item-created">' + comment.created_at + '</div>';
                    comments += '</div>';

                    comments += '<div class="comment-item-body"><span>' + comment.content + '</span><div><i class="fas fa-trash-alt delete-comment" title="Delete Comment" data-comment-id="' + comment._id + '"></i></div></div>';

                    comments += '</div>';

                    comment_count++;
                });

                if (comments !== '') comments += '<script type="text\/javascript">$(\'.delete-comment\').click(function() { deleteComment($(this).data("commentId")); });<\/script>';

                $('.comments-counter').html('<i class="far fa-comment-dots"></i><span>' + comment_count + ' COMMENTS</span>');
                $('.comments-list').html(comments);
            }

            $('.drawer-spinner-container').hide();
            $('.issue-container').fadeIn(300, function() {

                $('.issue-title-container > input').focus();
            });

            if (refreshData) {

                $('.drawer-spinner-container').hide();
                $('.issue-container').fadeIn(300, function() {

                    $('.issue-title-container > input').focus();
                });
            }

        }, 'json');
    }

    if (!refreshData) showDrawer(callback, true);
    else callback();
}

function saveIssue() {

    if (validateIssueFields()) {

        showModal('question', 'Save', 'Save changes?', function(result) {

            if (result) {

                showProgress('Saving...');

                var data = getIssueData();

                $.post(current_url + '/save', {data : data}, function(data, status, xhr) {

                    if (status == 'success') {

                        hideModal();

                        if (typeof (data.error) !== 'undefined') {

                            showMessage('error', data.error);
                            return false;
                        }

                        viewIssue(data.issue_id, true);
                        showList(filter);
                        showMessage('success', 'Changes successfully saved.', true);
                    }
                }, 'json');
            }
        });
    }
}

function showDrawer(callback, reopen = false) {

    if (reopen) $('.drawer').hide();

    if (!$('.drawer').is(':visible')) {

        if (!$('.submit-comment').hasClass('disabled')) $('.submit-comment').addClass('disabled');

        $('.new-comment').val('');
        $('.issue-type-list').hide();
        $('.issue-container').hide();
        $('.drawer-spinner-container').show();

        $('.drawer').animate({width:'toggle'}, 400, callback);
    }
}

function hideDrawer() {

    if ($('.drawer').is(':visible')) {

        $('.issue-type-list').hide();
        $('.issue-priority-list').hide();
        $('.issue-container').hide();
        $('.drawer').animate({width: 'toggle'}, 300);
    }
}

function submitComment() {

    var comment = $('.new-comment').val().trim();

    if (comment !== '') {

        showModal('question', 'Submit', 'Submit comment?', function(result) {

            if (result) {

                showProgress('Submitting...');

                var issue_id = $('.issue-id').text();

                $.post(current_url + '/comment/submit', {issue_id : issue_id, content : comment}, function(data, status, xhr) {

                    if (status == 'success') {

                        hideModal();

                        if (typeof (data.error) !== 'undefined') {

                            showMessage('error', data.error);
                            return false;
                        }

                        $('.new-comment').val('');
                        if (!$('.submit-comment').hasClass('disabled')) $('.submit-comment').addClass('disabled');

                        viewIssue(issue_id, true);
                        showMessage('success', 'Comment submitted.', true);
                    }
                }, 'json');
            }
        });
    }
}

function deleteComment(comment_id) {

    showModal('question', 'Delete', 'Are you sure to delete this comment?', function(result) {

        if (result) {

            showProgress('Deleting...');

            $.post(current_url + '/comment/delete', {comment_id : comment_id}, function(data, status, xhr) {

                if (status == 'success') {

                    hideModal();

                    if (typeof (data.error) !== 'undefined') {

                        showMessage('error', data.error);
                        return false;
                    }

                    viewIssue($('.issue-id-container > div').text().trim(), true);
                    showMessage('success', 'Comment successfully deleted.', true);
                }

            }, 'json');
        }
    });
}

function getIssueData() {

    var data = {};

    data = {
        issue_id    :   $('.issue-id').text(),
        title       :   $('.issue-title').val().trim(),
        description :   $('.issue-description').val(),
        type        :   $('.issue-type').val(),
        priority    :   $('.issue-priority').val()
    };

    return data;
}

function validateIssueFields() {

    var data = getIssueData();
    var fields = ['title', 'type', 'priority'];
    var field = '';

    $.each(fields, function(index, _field) {

        if (data[_field] == '') {

            field = _field;
            return false;
        }
    });

    if (field != '') {

        var message = 'Issue <field> is required.';
        var selector = '.issue-' + field;

        if (['type','priority'].indexOf(field) >= 0) selector += '-box-container';

        message = message.replace('<field>', field);
        showMessage('error', message, true);

        $(selector).focus();
    }

    return field == '';
}
