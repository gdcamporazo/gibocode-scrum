var current_url = window.location.href;
var filter = {};

$(document).ready(function() {

    hideMessage();
    showList(filter);

    setSearchAction(function() {

        showList(filter);
    });
});

function showList(filter = {}) {

    $('.users-counter').hide();
    $('.users-container').hide();

    showSpinner();

    $.post(current_url + '/list', filter, function(data, status, xhr) {

        if (status == 'success') {

            if (typeof (data.error) !== 'undefined') {

                showMessage('error', data.error);
                return false;
            }

            var users = '';
            var count = 0;

            $.each(data, function(index, item) {

                users += '<div class="user-item" data-user-id="' + item.id + '">';

                users += '<div class="user-profile-image">' + getProfileImage(item.id) + '</div>';

                users += '<div class="user-info">';
                users += '<div class="user-fullname">' + item.fullname;

                if (item.username == $('#current-user').text().trim()) {

                    users += '<i class="fas fa-check-circle" title="Current User"></i>';
                }

                users += '</div>';
                users += '<div class="user-username">' + item.username + '</div>';
                users += '<div class="user-email">' + item.email + '</div>';
                users += '</div>';

                users += '</div>';

                count++;
            });

            if (users == '') users = '<span style="color:#e06666;position:relative;top:10px;">No users found.</span>';
            else users += '<script type="text\/javascript">$(\'.user-item\').click(function() { openUserProfile($(this).data("userId")); });<\/script>';

            hideSpinner();

            if (count > 0) $('.users-counter').html(count + ' Users').fadeIn();

            $('.users-container').html(users).fadeIn();
        }
    }, 'json');
}

function getProfileImage() {

    var image = '<i class="fas fa-user-circle"></i>';

    return image;
}

function openUserProfile(id) {

    window.location.href = current_url + '/user/id/' + id;
}
